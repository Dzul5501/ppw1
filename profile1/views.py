from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.

def index(request):
    return render(request, 'page1.html')

def index2(request):
    return render(request, 'page2.html')

def index3(request):
    return render(request, 'page3.html')

def submit(request):
    return render(request, 'submit.html')
